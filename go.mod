module github.com/asticode/go-astilectron-demo

go 1.13

require (
	github.com/asticode/go-astichartjs v0.1.0
	github.com/asticode/go-astikit v0.15.0
	github.com/asticode/go-astilectron v0.24.0
	github.com/asticode/go-astilectron-bootstrap v0.4.12
	github.com/asticode/go-astilectron-bundler v0.7.11 // indirect
)
